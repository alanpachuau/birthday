var app = require('app')
var ipc = require('ipc')
var BrowserWindow = require('browser-window')
var Datastore = require('nedb')
var moment = require('moment')
searchTerm = ''

Friends = new Datastore({ filename: __dirname + '/../database/friends.db', autoload: true })
Profile = new Datastore({ filename: __dirname + '/../database/profile.db', autoload: true })
Settings = new Datastore({ filename: __dirname + '/../database/settings.db', autoload: true })

var windowWidth = 1000
var windowHeight = 1000

var windowConfig = {width:1000, height: 600, show: false}
electron = require('electron')
// Module to control application life.
app = electron.app
// Module to create native browser window.
BrowserWindow = electron.BrowserWindow

var mainWindow

Menu = electron.Menu

var ipc = electron.ipcMain
var Datastore = require('nedb')
var moment = require('moment')

Friends = new Datastore({ filename: __dirname + '/../database/friends.db', autoload: true })
Profile = new Datastore({ filename: __dirname + '/../database/profile.db', autoload: true })

var windowWidth = 1040
var windowHeight = 600

var windowConfig = {width:1040, height: 600, show: false}

var pages = {
    profile: 'file://' + __dirname + '/profile.html',
    setupProfile: 'file://' + __dirname + '/setupProfile.html',
    friends: 'file://' + __dirname + '/friends.html',
    birthdayToday: 'file://' + __dirname + '/birthdayToday.html',
    birthdayTomorrow: 'file://' + __dirname + '/birthdayTomorrow.html',
    birthdayThisWeek: 'file://' + __dirname + '/birthdayThisWeek.html',
    birthdayNextWeek: 'file://' + __dirname + '/birthdayNextWeek.html',
    birthdayThisMonth: 'file://' + __dirname + '/birthdayThisMonth.html',
    birthdayNextMonth: 'file://' + __dirname + '/birthdayNextMonth.html'
}

require('crash-reporter').start();

var template = [
  {
      label: 'Birthday',
      submenu: [
          {
              label: 'Friends',
              selector: 'friends:',
              click: function () {
                mainWindow.loadURL(pages.friends)
              }
          },
          {
              label: 'Profile',
              selector: 'profile:',
              click: function () {
                mainWindow.loadURL(pages.profile)
              }
          },
          {
              label: 'Birthday Today',
              selector: 'birthdayToday:',
              click: function () {
                  mainWindow.loadURL(pages.birthdayToday);
              }
          },
          {
              label: 'Birthday Tomorrow',
              selector: 'birthdayTomorrow:',
              click: function () {
                  mainWindow.loadURL(pages.birthdayTomorrow);
              }
          },
          {
              label: 'Birthday This Month',
              selector: 'birthdayThisMonth:',
              click: function () {
                  mainWindow.loadURL(pages.birthdayThisMonth);
              }
          },
          {
              label: 'Birthday Next Month',
              selector: 'birthdayNextMonth:',
              click: function () {
                  mainWindow.loadURL(pages.birthdayNextMonth);
              }
          }
      ]
  },
  {
    label: 'Edit',
      submenu: [
        {
          label: 'Undo',
          accelerator: 'CmdOrCtrl+Z',
          role: 'undo'
        }, {
          label: 'Redo',
          accelerator: 'Shift+CmdOrCtrl+Z',
          role: 'redo'
        }, {
          type: 'separator'
        }, {
          label: 'Cut',
          accelerator: 'CmdOrCtrl+X',
          role: 'cut'
        }, {
          label: 'Copy',
          accelerator: 'CmdOrCtrl+C',
          role: 'copy'
        }, {
          label: 'Paste',
          accelerator: 'CmdOrCtrl+V',
          role: 'paste'
        }, {
          label: 'Select All',
          accelerator: 'CmdOrCtrl+A',
          role: 'selectall'
        }
      ]
    }, {
    label: 'View',
    submenu: [
      {
        label: 'Reload',
        accelerator: 'CmdOrCtrl+R',
        click: function (item, focusedWindow) {
          if (focusedWindow) {
            // on reload, start fresh and close any old
            // open secondary windows
            if (focusedWindow.id === 1) {
              BrowserWindow.getAllWindows().forEach(function (win) {
                if (win.id > 1) {
                  win.close()
                }
              })
            }
            focusedWindow.reload()
          }
        }
      }, {
        label: 'Toggle Full Screen',
        accelerator: (function () {
          if (process.platform === 'darwin') {
            return 'Ctrl+Command+F'
          } else {
            return 'F11'
          }
        })(),
        click: function (item, focusedWindow) {
          if (focusedWindow) {
            focusedWindow.setFullScreen(!focusedWindow.isFullScreen())
          }
        }
      }, {
        label: 'Toggle Developer Tools',
        accelerator: (function () {
          if (process.platform === 'darwin') {
            return 'Alt+Command+I'
          } else {
            return 'Ctrl+Shift+I'
          }
        })(),
        click: function (item, focusedWindow) {
          if (focusedWindow) {
            focusedWindow.toggleDevTools()
          }
        }
      }
    ]
    }, {
      label: 'Window',
      role: 'window',
      submenu: [{
        label: 'Minimize',
        accelerator: 'CmdOrCtrl+M',
        role: 'minimize'
      }, {
        label: 'Close',
        accelerator: 'CmdOrCtrl+W',
        role: 'close'
      }, {
        type: 'separator'
      }, {
        label: 'Reopen Window',
        accelerator: 'CmdOrCtrl+Shift+T',
        enabled: false,
        key: 'reopenMenuItem',
        click: function () {
          app.emit('activate')
        }
      }]
    },
    {
        label: 'Help',
        submenu: []
    },
];

app.on('window-all-closed', function() {
    if(process.platform != 'darwin') {
        app.quit()
    }
})

app.on('ready', function() {

    // Main window
    mainWindow = new BrowserWindow(windowConfig)
    mainWindow.on('closed', function() {
        mainWindow = null
        app.quit()
    })

    // Get profile
    Profile.find({}, function(err, profiles){
        // If profile exists show main window
        if(profiles.length > 0) {
            const menu = Menu.buildFromTemplate(template)
            Menu.setApplicationMenu(menu)

            mainWindow.loadURL(pages.friends)
            mainWindow.show()
        }
        // If profile does not exists show profile window
        else {
            mainWindow.setSize(windowWidth, windowHeight)
            mainWindow.loadURL(pages.setupProfile)
            mainWindow.show()
        }
    })

    ipc.on('saveProfile', function(event, name, birthday) {
        Profile.insert({
            name: name,
            birthday: moment(birthday, 'D MMMM YYYY').startOf("day").toDate().toJSON(),
            created: moment().toDate().toJSON(),
            updated: moment().toDate().toJSON()
        }, function(error, newProfile) {
            console.log(newProfile)

            Settings.insert({key: 'profileId', value: newProfile._id})
            Settings.insert({key: 'desktopNotification', value: 0})
        })

        mainWindow.setSize(windowWidth, windowHeight)
        mainWindow.loadUrl(pages.friends)
    })

    ipc.on('updateProfile', function(event, docId, name, birthday) {
        Profile.update(
            {_id: docId},
            {
                $set: {
                    name: name,
                    birthday: moment(birthday, 'D MMMM YYYY').startOf("day").toDate().toJSON(),
                    updated: moment().toDate().toJSON()
                }
            },
            { multi: true },
            function(error, numReplaced) {
                console.log(numReplaced)
            }
        )

        mainWindow.setSize(windowWidth, windowHeight)
        mainWindow.loadUrl(pages.profile)
    })

    ipc.on('createFriend', function(event, name, father, sex, birthday) {
        Friends.insert({
            name: name,
            father: father,
            sex: sex,
            birthday: moment(birthday, 'D MMMM YYYY').startOf("day").toDate().toJSON(),
            year: moment(birthday, 'D MMMM YYYY').year(),
            month: moment(birthday, 'D MMMM YYYY').month(),
            day: moment(birthday, 'D MMMM YYYY').date(),
            created: moment().toDate().toJSON(),
            updated: moment().toDate().toJSON()
        }, function(error, newProfile) {
            console.log(newProfile)
        })

        mainWindow.setSize(windowWidth, windowHeight)
        mainWindow.loadUrl(pages.friends)
    })

    ipc.on('updateFriend', function(event, docId, name, father, sex, birthday) {
        Friends.update(
            {_id: docId},
            {
                $set: {
                    name: name,
                    father: father,
                    sex: sex,
                    birthday: moment(birthday, 'D MMMM YYYY').startOf("day").toDate().toJSON(),
                    year: moment(birthday, 'D MMMM YYYY').year(),
                    month: moment(birthday, 'D MMMM YYYY').month(),
                    day: moment(birthday, 'D MMMM YYYY').date(),
                    updated: moment().toDate().toJSON()
                }
            },
            { multi: true },
            function(error, numReplaced) {
                console.log(numReplaced)
            }
        )

        mainWindow.setSize(windowWidth, windowHeight)
        mainWindow.loadUrl(pages.friends)
    })

    ipc.on('deleteFriend', function(event, docId) {
        Friends.remove({ _id: docId }, {}, function (err, numRemoved) {

        });

        //mainWindow.setSize(windowWidth, windowHeight)
        //mainWindow.loadUrl(pages.friends)
    })

    ipc.on('friends', function(){
        mainWindow.setSize(windowWidth, windowHeight)
        mainWindow.loadUrl(pages.friends)
    })

    ipc.on('profile', function(){
        mainWindow.setSize(windowWidth, windowHeight)
        mainWindow.loadUrl(pages.profile)
    })

    ipc.on('birthdayToday', function(){
        mainWindow.setSize(windowWidth, windowHeight)
        mainWindow.loadUrl(pages.birthdayToday)
    })

    ipc.on('birthdayTomorrow', function(){
        mainWindow.setSize(windowWidth, windowHeight)
        mainWindow.loadUrl(pages.birthdayTomorrow)
    })

    ipc.on('birthdayThisWeek', function(){
        mainWindow.setSize(windowWidth, windowHeight)
        mainWindow.loadUrl(pages.birthdayThisWeek)
    })

    ipc.on('birthdayNextWeek', function(){
        mainWindow.setSize(windowWidth, windowHeight)
        mainWindow.loadUrl(pages.birthdayNextWeek)
    })

    ipc.on('birthdayThisMonth', function(){
        mainWindow.setSize(windowWidth, windowHeight)
        mainWindow.loadUrl(pages.birthdayThisMonth)
    })

    ipc.on('birthdayNextMonth', function(){
        mainWindow.setSize(windowWidth, windowHeight)
        mainWindow.loadUrl(pages.birthdayNextMonth)
    })

    ipc.on('searchFriend', function(event, term){
        searchTerm = term;
        mainWindow.setSize(windowWidth, windowHeight)
        mainWindow.loadUrl(pages.friends)
    })
})

ipc.on('saveProfile', function(event, name, birthday, desktopNotification) {
    Profile.insert({
        name: name,
        birthday: moment(birthday, 'D MMMM YYYY').startOf("day").toDate().toJSON(),
        desktopNotification: desktopNotification,
        created: moment().toDate().toJSON(),
        updated: moment().toDate().toJSON()
    }, function(error, newProfile) {
        const menu = Menu.buildFromTemplate(template)
        Menu.setApplicationMenu(menu)

        mainWindow.loadURL(pages.friends)
        mainWindow.show()

        console.log(newProfile)
    })
})

ipc.on('updateProfile', function(event, docId, name, birthday, desktopNotification) {
    Profile.update(
        {_id: docId},
        {
            $set: {
                name: name,
                desktopNotification: desktopNotification,
                birthday: moment(birthday, 'D MMMM YYYY').startOf("day").toDate().toJSON(),
                updated: moment().toDate().toJSON()
            }
        },
        { multi: true },
        function(error, numReplaced) {
            console.log(numReplaced)
            event.sender.send('profileUpdated', numReplaced);
        }
    )
})

ipc.on('createFriend', function(event, name, father, sex, birthday) {
    Friends.insert({
        name: name,
        father: father,
        sex: sex,
        birthday: moment(birthday, 'D MMMM YYYY').startOf("day").toDate().toJSON(),
        year: moment(birthday, 'D MMMM YYYY').year(),
        month: moment(birthday, 'D MMMM YYYY').month(),
        day: moment(birthday, 'D MMMM YYYY').date(),
        created: moment().toDate().toJSON(),
        updated: moment().toDate().toJSON()
    }, function(error, newProfile) {
        console.log(newProfile)
        event.sender.send('friendAdded', newProfile._id);
    })
})

ipc.on('updateFriend', function(event, docId, name, father, sex, birthday) {
    Friends.update(
        {_id: docId},
        {
            $set: {
                name: name,
                father: father,
                sex: sex,
                birthday: moment(birthday, 'D MMMM YYYY').startOf("day").toDate().toJSON(),
                year: moment(birthday, 'D MMMM YYYY').year(),
                month: moment(birthday, 'D MMMM YYYY').month(),
                day: moment(birthday, 'D MMMM YYYY').date(),
                updated: moment().toDate().toJSON()
            }
        },
        { multi: true },
        function(error, numReplaced) {
            console.log(numReplaced)
            event.sender.send('friendUpdated', docId);
        }
    )

})

ipc.on('deleteFriend', function(event, docId) {
    Friends.remove({ _id: docId }, {}, function (err, numRemoved) {
      event.sender.send('friendRemoved', docId);
    });
})

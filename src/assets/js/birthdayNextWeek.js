jQuery(function($){
    var dayInWeek = moment().day(); // From 0 to 6
    var daysAfter = 6 - dayInWeek + 1;
    var weekStart = moment().add(daysAfter, 'days').date()
    var weekEnd = moment().add(daysAfter+6, 'days').date()

    Friends.find({ month: moment().month(), day: {$gte: weekStart, $lte: weekEnd} }).sort({ day: 1 }).limit(100).exec(function (err, docs) {
        $.each(docs, function(key, data){
            var friendCard = '<div id="' + data._id + '_card" class="card"><div class="content">'
            friendCard += '<div class="header">' + data.name + '</div>'
            friendCard += '<div class="meta">'

            if(data.sex == 'Female')
                friendCard += 'd/o '
            else if(data.sex == 'Male')
                friendCard += 's/o '

            friendCard += data.father + '</div>'
            friendCard += '<div class="description">'
            friendCard += '<div class="dob">Date of Birth: ' + moment(data.birthday).format("D MMMM, YYYY") + '</div>'
            friendCard += '<div class="sex">Sex: ' + data.sex + '</div></div></div>'
            friendCard += '</div>'

            $("#friends").append(friendCard);
        })

        if(docs.length <= 0)
            $("#friends").append('<div class="ui icon message"><i class="warning sign icon"></i><div class="content"><div class="header">Alert</div><p>No birthday next week</p></div></div>')
    })
})
var Menu = remote.require('menu')
var MenuItem = remote.require('menu-item')

var template = [
    {
        label: 'Electron',
        submenu: [
            {
                label: 'About Electron',
                selector: 'orderFrontStandardAboutPanel:'
            },
            {
                type: 'separator'
            },
            {
                label: 'Services',
                submenu: []
            },
            {
                type: 'separator'
            },
            {
                label: 'Hide Electron',
                accelerator: 'Command+H',
                selector: 'hide:'
            },
            {
                label: 'Hide Others',
                accelerator: 'Command+Shift+H',
                selector: 'hideOtherApplications:'
            },
            {
                label: 'Show All',
                selector: 'unhideAllApplications:'
            },
            {
                type: 'separator'
            },
            {
                label: 'Quit',
                accelerator: 'Command+Q',
                click: function () {
                    remote.require('app').quit();
                }
            },
        ]
    },
    {
        label: 'Menu',
        submenu: [
            {
                label: 'Friends',
                selector: 'friends:',
                click: function () {
                    ipc.send('friends');
                }
            },
            {
                label: 'Profile',
                selector: 'profile:',
                click: function () {
                    ipc.send('profile');
                }
            },
            {
                label: 'Birthday Today',
                selector: 'birthdayToday:',
                click: function () {
                    ipc.send('birthdayToday');
                }
            },
            {
                label: 'Birthday Tomorrow',
                selector: 'birthdayTomorrow:',
                click: function () {
                    ipc.send('birthdayTomorrow');
                }
            },
            /*{
                label: 'Birthday This Week',
                selector: 'birthdayThisWeek:',
                click: function () {
                    ipc.send('birthdayThisWeek');
                }
            },
            {
                label: 'Birthday Next Week',
                selector: 'birthdayNextWeek:',
                click: function () {
                    ipc.send('birthdayNextWeek');
                }
            },*/
            {
                label: 'Birthday This Month',
                selector: 'birthdayThisMonth:',
                click: function () {
                    ipc.send('birthdayThisMonth');
                }
            },
            {
                label: 'Birthday Next Month',
                selector: 'birthdayNextMonth:',
                click: function () {
                    ipc.send('birthdayNextMonth');
                }
            }
        ]
    },
    {
        label: 'Edit',
        submenu: [
            {
                label: 'Undo',
                accelerator: 'Command+Z',
                selector: 'undo:'
            },
            {
                label: 'Redo',
                accelerator: 'Shift+Command+Z',
                selector: 'redo:'
            },
            {
                type: 'separator'
            },
            {
                label: 'Cut',
                accelerator: 'Command+X',
                selector: 'cut:'
            },
            {
                label: 'Copy',
                accelerator: 'Command+C',
                selector: 'copy:'
            },
            {
                label: 'Paste',
                accelerator: 'Command+V',
                selector: 'paste:'
            },
            {
                label: 'Select All',
                accelerator: 'Command+A',
                selector: 'selectAll:'
            },
        ]
    },
    {
        label: 'View',
        submenu: [
            {
                label: 'Reload',
                accelerator: 'Command+R',
                click: function (item, focusedWindow) {
                    if(focusedWindow)
                        focusedWindow.reloadIgnoringCache();
                }
            },
            {
                label: 'Toggle DevTools',
                accelerator: (function () {
                    if (process.platform == 'darwin')
                        return 'Alt+Command+I';
                    else
                        return 'Ctrl+Shift+I';
                })(),
                click: function (item, focusedWindow) {
                    if (focusedWindow)
                        focusedWindow.toggleDevTools();
                }
            },
        ]
    },
    {
        label: 'Window',
        submenu: [
            {
                label: 'Minimize',
                accelerator: 'Command+M',
                selector: 'performMiniaturize:'
            },
            {
                label: 'Close',
                accelerator: 'Command+W',
                selector: 'performClose:'
            },
            {
                type: 'separator'
            },
            {
                label: 'Bring All to Front',
                selector: 'arrangeInFront:'
            },
        ]
    },
    {
        label: 'Help',
        submenu: []
    },
];

menu = Menu.buildFromTemplate(template);

Menu.setApplicationMenu(menu);
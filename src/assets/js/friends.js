<<<<<<< HEAD
jQuery(function($){

=======
var searchQueue;

jQuery(function($){
>>>>>>> 0a6125e16747296c876184e3d7cc0a67ae2cff92
    var $input1 = $('.add-friend .birthday .datepicker').pickadate({
        selectYears: 20,
        selectMonths: true,
        format: 'd mmmm, yyyy',
        min: moment('1 January, 1980', 'D MMMM, YYYY').toDate()
    })
    picker1 = $input1.pickadate('picker')
    var $input2 = $('.update-friend .birthday .datepicker').pickadate({
        selectYears: 20,
        selectMonths: true,
        format: 'd mmmm, yyyy',
        min: moment('1 January, 1980', 'D MMMM, YYYY').toDate()
    })
    picker2 = $input2.pickadate('picker')

    $("#addFriend").on("click", function(){
<<<<<<< HEAD
=======
        document.getElementById('addFriendForm').reset();
>>>>>>> 0a6125e16747296c876184e3d7cc0a67ae2cff92
        $('.ui.modal.add-friend').modal('show');
    })

    $("#createFriend").on("click", function(e){
        var name = $("#addFriendForm .field.name input").val()
        var father = $("#addFriendForm .field.father input").val()
        var sex = $("#addFriendForm .field.sex select").val()
        var birthday = $("#addFriendForm .field.birthday input").val()

        e.preventDefault(true)
        $('#addFriendForm').addClass('loading')
        $("#addFriendForm .field.name, #addFriendForm .field.father, #addFriendForm .field.birthday")
            .removeClass('error')

        var valid = true;

        if(validator.isNull(name)) {
            $("#addFriendForm .field.name").addClass('error')
            valid = false;
        }

        if(validator.isNull(father)) {
            $("#addFriendForm .field.father").addClass('error')
            valid = false;
        }

        if( validator.isNull(birthday) || !validator.isDate(birthday) ) {
            $("#addFriendForm .field.birthday").addClass('error')
            valid = false;
        }

        if(valid)
            ipc.send('createFriend', name, father, sex, birthday)
<<<<<<< HEAD
        else
            $('#addFriendForm').removeClass('loading')
    })

    $("#searchFriend").on("click", function(e){
        var searchTerm = $("#searchFriendForm .field input").val()

        e.preventDefault(true)

        ipc.send('searchFriend', searchTerm)
=======

        $('#addFriendForm').removeClass('loading')
    })

    $("#searchFriend").on("click", function(e){
        e.preventDefault(true)
        loadFriends();
>>>>>>> 0a6125e16747296c876184e3d7cc0a67ae2cff92
    })

    $("#searchFriendForm").on("submit", function(evt){
        evt.preventDefault(true);
<<<<<<< HEAD
        var searchTerm = $("#searchFriendForm .field input").val()
        ipc.send('searchFriend', searchTerm)
        return false;
    })

    Friends.find({}).sort({ name: 1 }).limit(100).exec(function (err, docs) {
        var searchTerm = remote.getGlobal('searchTerm');
        if(searchTerm.length > 0) {
            $("#searchFriendForm .field input").val(searchTerm)
        }

        $.each(docs, function(key, data){
            var skip = true;
            var exp = '';
            if(searchTerm.length > 0) {
                exp = new RegExp(searchTerm, "gi")
                var exp = new RegExp(searchTerm, "gi")
                var matchName = data.name.match(exp);
                var matchFather = data.father.match(exp);
                if(matchName || matchFather)
                    skip = false;
            }
            else {
                skip = false;
            }

            if(!skip) {
                var friendCard = '<div id="' + data._id + '_card" class="card"><div class="content">'
                friendCard += '<div class="header">' + data.name.replace(exp, "<span>"+searchTerm+"</span>") + '</div>'
                friendCard += '<div class="meta">'

                if (data.sex == 'Female')
                    friendCard += 'd/o '
                else if (data.sex == 'Male')
                    friendCard += 's/o '

                friendCard += data.father.replace(exp, "<span>"+searchTerm+"</span>") + '</div>'
                friendCard += '<div class="description">'
                friendCard += '<div class="dob">Date of Birth: ' + moment(data.birthday).format("D MMMM, YYYY") + '</div>'
                friendCard += '<div class="sex">Sex: ' + data.sex + '</div></div></div>'
                friendCard += '<div class="extra content"><div class="ui two buttons">'
                friendCard += '<button class="ui basic green button edit-friend" data-id="' + data._id + '">Edit</button>'
                friendCard += '<button id="' + data._id + '_delete_button" class="ui basic red button" data-id="' + data._id + '">Delete</button>'
                friendCard += '</div></div>'
                friendCard += '<div id="' + data._id + '_popup" class="ui custom popup top left transition hidden"><p>Are you sure?</p>'
                friendCard += '<button class="ui mini green button cancel-delete-friend" data-id="' + data._id + '">No</button>'
                friendCard += '<button class="ui mini red button delete-friend" data-id="' + data._id + '">Yes</button>'
                friendCard += '</div></div>'

                $("#friends").append(friendCard);

                $('#' + data._id + '_delete_button').popup({
                    popup: $('#' + data._id + '_popup'),
                    on: 'click'
                })
            }
        })


        $(".edit-friend").on("click", function(e){
            var docId = $(this).data('id')
            Friends.findOne({_id: docId}, function(err, doc){
                $(".ui.modal.update-friend .display-name").text(doc.name);
                $("#updateFriendForm input#docId").val(doc._id)
                $("#updateFriendForm .field.name input").val(doc.name)
                $("#updateFriendForm .field.father input").val(doc.father)
                $("#updateFriendForm .field.sex select").val(doc.sex)
                picker2.set('select', moment(doc.birthday).format('D MMMM, YYYY'), {format: 'd mmmm, yyyy'})

                $('.ui.modal.update-friend').modal('show')
                $('.ui.dropdown').dropdown()
            })
        })

        $(".cancel-delete-friend").on("click", function(){
            var docId = $(this).data('id')
            $('#' + docId + '_delete_button').popup('hide all');
        })

        $(".delete-friend").on("click", function(){
            var docId = $(this).data('id')

            $("#" + docId + "_card").fadeOut(function(){
                ipc.send('deleteFriend', docId)
            });
        });

        $("#updateFriend").on("click", function(e){
            var docId = $("#updateFriendForm input#docId").val()
            var name = $("#updateFriendForm .field.name input").val()
            var father = $("#updateFriendForm .field.father input").val()
            var sex = $("#updateFriendForm .field.sex select").val()
            var birthday = $("#updateFriendForm .field.birthday input").val()

            e.preventDefault(true)
            $('#updateFriendForm').addClass('loading')
            $("#updateFriendForm .field.name, #updateFriendForm .field.father, #updateFriendForm .field.birthday")
                .removeClass('error')

            var valid = true;

            if(validator.isNull(name)) {
                $("#updateFriendForm .field.name").addClass('error')
                valid = false;
            }

            if(validator.isNull(father)) {
                $("#updateFriendForm .field.father").addClass('error')
                valid = false;
            }

            if( validator.isNull(birthday) || !validator.isDate(birthday) ) {
                $("#updateFriendForm .field.birthday").addClass('error')
                valid = false;
            }

            if(valid)
                ipc.send('updateFriend', docId, name, father, sex, birthday)
            else
                $('#updateFriendForm').removeClass('loading')
        })
    });
})
=======
        loadFriends();
        return false;
    })

    $("body").on("click", ".edit-friend", function(e){
        var docId = $(this).data('id')
        Friends.findOne({_id: docId}, function(err, doc){
            $(".ui.modal.update-friend .display-name").text(doc.name);
            $("#updateFriendForm input#docId").val(doc._id)
            $("#updateFriendForm .field.name input").val(doc.name)
            $("#updateFriendForm .field.father input").val(doc.father)
            $("#updateFriendForm .field.sex select").val(doc.sex)
            picker2.set('select', moment(doc.birthday).format('D MMMM, YYYY'), {format: 'd mmmm, yyyy'})

            $('.ui.modal.update-friend').modal('show')
            $('.ui.dropdown').dropdown()
        })
    })

    $("body").on("click", ".cancel-delete-friend", function(){
        var docId = $(this).data('id')
        $('#' + docId + '_delete_button').popup('hide all');
    })

    $("body").on("click", ".delete-friend", function(){
        var docId = $(this).data('id')
        $('#' + docId + '_delete_button').popup('hide all');
        ipc.send('deleteFriend', docId)
    });

    $("body").on("click", "#updateFriend", function(e){
        var docId = $("#updateFriendForm input#docId").val()
        var name = $("#updateFriendForm .field.name input").val()
        var father = $("#updateFriendForm .field.father input").val()
        var sex = $("#updateFriendForm .field.sex select").val()
        var birthday = $("#updateFriendForm .field.birthday input").val()

        e.preventDefault(true)
        $('#updateFriendForm').addClass('loading')
        $("#updateFriendForm .field.name, #updateFriendForm .field.father, #updateFriendForm .field.birthday")
            .removeClass('error')

        var valid = true;

        if(validator.isNull(name)) {
            $("#updateFriendForm .field.name").addClass('error')
            valid = false;
        }

        if(validator.isNull(father)) {
            $("#updateFriendForm .field.father").addClass('error')
            valid = false;
        }

        if( validator.isNull(birthday) || !validator.isDate(birthday) ) {
            $("#updateFriendForm .field.birthday").addClass('error')
            valid = false;
        }

        if(valid)
            ipc.send('updateFriend', docId, name, father, sex, birthday)

        $('#updateFriendForm').removeClass('loading')
    })

    $("#searchFriendForm .field input").on("keyup", function(){
      clearTimeout(searchQueue);
      searchQueue = setTimeout(function(){
        loadFriends();
      }, 200);
    });

    loadFriends();
})

function loadFriends() {
  $("#friends").html("");
  Friends.find({}).sort({ name: 1 }).limit(100).exec(function (err, docs) {
      var searchTerm = $("#searchFriendForm .field input").val();

      $.each(docs, function(key, data){
          var skip = true;
          var exp = '';
          if(searchTerm.length > 0) {
              exp = new RegExp(searchTerm, "gi")
              var exp = new RegExp(searchTerm, "gi")
              var matchName = data.name.match(exp);
              var matchFather = data.father.match(exp);
              if(matchName || matchFather)
                  skip = false;
          }
          else {
              skip = false;
          }

          if(!skip) {
              var friendCard = '<div id="' + data._id + '_card" class="ui card" style="display:none;"><div class="content">'
              friendCard += '<div class="header">' + data.name.replace(exp, "<span>"+searchTerm+"</span>") + '</div>'
              friendCard += '<div class="meta">'

              if (data.sex == 'Female')
                  friendCard += 'd/o '
              else if (data.sex == 'Male')
                  friendCard += 's/o '

              friendCard += data.father.replace(exp, "<span>"+searchTerm+"</span>") + '</div>'
              friendCard += '<div class="description">'
              friendCard += '<div class="dob">' + moment(data.birthday).format("dddd, Do MMMM YYYY") + '</div>'
              // friendCard += '<div class="sex">Sex: ' + data.sex + '</div>'
              friendCard += '</div></div>'
              friendCard += '<div class="extra content">'
              friendCard += '<span class="right floated like edit-friend" data-id="' + data._id + '"><i class="write icon"></i>Edit</span>'
              friendCard += '<span id="' + data._id + '_delete_button" class="left floated like delete-friend-prompt" data-id="' + data._id + '"><i class="remove circle icon"></i>Delete</span>'
              friendCard += '</div>'
              // friendCard += '<div class="extra content"><div class="ui two buttons">'
              // friendCard += '<button class="ui basic green button edit-friend" data-id="' + data._id + '">Edit</button>'
              // friendCard += '<button id="' + data._id + '_delete_button" class="ui basic red button" data-id="' + data._id + '">Delete</button>'
              // friendCard += '</div></div>'
              friendCard += '<div id="' + data._id + '_popup" class="ui custom popup top left transition hidden"><p>Are you sure?</p>'
              friendCard += '<button class="ui mini green button cancel-delete-friend" data-id="' + data._id + '">No</button>'
              friendCard += '<button class="ui mini red button delete-friend" data-id="' + data._id + '">Yes</button>'
              friendCard += '</div></div>'

              $("#friends").append(friendCard);

              $('#' + data._id + '_card')
                .transition('slide right')
                .transition('pulse');

              $('#' + data._id + '_delete_button').popup({
                  popup: $('#' + data._id + '_popup'),
                  on: 'click'
              })
          }
      })
  });
}

ipc.on('friendAdded', function(event, newId){
  console.log(newId);
  loadFriends();
  tada('#'+newId+'_card');
})

ipc.on('friendUpdated', function(event, friendId){
  console.log(friendId);
  loadFriends();
  jiggle('#'+friendId+'_card');
})

ipc.on('friendRemoved', function(event, friendId){
  console.log(friendId);
  flash('#'+friendId+'_card');
})

function tada(id)
{
  setTimeout(function(){
    $(id)
      .transition({
        animation  : 'tada'
      });
  }, 300);
}

function jiggle(id)
{
  setTimeout(function(){
    $(id)
      .transition({
        animation  : 'jiggle'
      });
  }, 300);
}

function flash(id)
{
  setTimeout(function(){
    $(id)
      .transition({
        animation  : 'flash',
        duration   : 400,
        onComplete: function(){
          $(id)
            .transition({
              animation  : 'slide right',
              duration   : 400,
              onComplete: function(){
                $(id).remove();
              }
            });
        }
      });
  }, 300);
}
>>>>>>> 0a6125e16747296c876184e3d7cc0a67ae2cff92

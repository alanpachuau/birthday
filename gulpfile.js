var gulp = require('gulp')
var gutil = require('gulp-util')

gulp.task('default', ['copy'])

gulp.task('copy', function() {
    gulp.src('./node_modules/jquery/dist/jquery.min.*').pipe(gulp.dest('./src/assets/js'))
    gulp.src('./node_modules/semantic-ui/dist/semantic.min.css').pipe(gulp.dest('./src/assets/css/semantic'))
    gulp.src('./node_modules/semantic-ui/dist/semantic.min.js').pipe(gulp.dest('./src/assets/js'))
    gulp.src('./node_modules/semantic-ui/dist/themes/default/**/*').pipe(gulp.dest('./src/assets/css/semantic/themes/default'))
    gulp.src('./node_modules/pickadate/lib/compressed/picker.*').pipe(gulp.dest('./src/assets/js'))
    gulp.src('./node_modules/pickadate/lib/compressed/themes/default.*').pipe(gulp.dest('./src/assets/css/pickadate'))
})

//gulp.task('copyDir', function() {
    //return gulp.src('./node_modules/pickadate/lib/compressed/**/*').pipe(gulp.dest('./assets/lib/'))
//})


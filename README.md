# Birthday Application

An application to store birthdays and display them in an easy to read fashion. Easily searchable interface.

## Packaging Windows
`electron-packager . Birthday --platform=win32 --arch=all --version=0.34.3 --overwrite --out=./output --ignore=node_modules`

## Packaging OS X
Get electron version
`electron -v`

`electron-packager . Birthday --platform=darwin --arch=x64 --version=0.34.3 --overwrite --out=./output --ignore=node_modules`
